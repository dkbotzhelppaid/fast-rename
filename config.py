from os import environ

API_ID = int(environ.get("API_ID", ""))
API_HASH = environ.get("API_HASH", "")
BOT_TOKEN = environ.get("BOT_TOKEN", "")
ADMIN = int(environ.get("ADMIN", ""))          
CAPTION = environ.get("CAPTION", "")
channel = environ.get("FORCE_CHANNEL", "") # ENTER YOUR CHANNEL USERNAME WITHOUT @
ft = environ.get("FORCE_SUB_TEXT", "**Due To Overload Only Channel Subscribers Can User This.\n\nKindly Join Our Update Channel 💘**")
LOG_CHANNEL = environ.get("LOG_CHANNEL", "")


class temp(object):
    THUMBNAIL = environ.get("THUMBNAIL", "")
